package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

var Driver *sql.DB

func posts(c *gin.Context) {
	type body struct {
		id     int
		Name   string
		Status bool
	}
	first, _ := c.GetRawData()
	var newformat body
	err := json.Unmarshal(first, &newformat)
	if err != nil {
		fmt.Print("error", err)
	}

	if _, err := Driver.Exec("CREATE TABLE IF NOT EXISTS TODOITEMS (id int, Name varchar(30), Status boolean, PRIMARY KEY(id));"); err != nil {
		c.String(http.StatusInternalServerError,
			fmt.Sprintf("Error creating database table: %q", err))
		return
	}
	statement := `INSERT INTO TODOITEMS(id,Name,Status) VALUES ($1,$2,$3)`
	if _, err := Driver.Exec(statement, newformat.id, newformat.Name, newformat.Status); err != nil {
		c.String(http.StatusInternalServerError,
			fmt.Sprintf("Error Adding item: %q", err))
		return
	}
	c.JSON(200, gin.H{})
}

func retrievevalue(db *sql.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		if _, err := Driver.Exec("SELECT * FROM TODOITEMS"); err != nil {
			c.String(http.StatusInternalServerError,
				fmt.Sprintf("Error retrieving value: %q", err))
			return
		}
	}
}
func deletevalue(db *sql.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		if _, err := Driver.Exec("DELETE FROM TODOITEMS WHERE Task_id=1"); err != nil {
			c.String(http.StatusInternalServerError,
				fmt.Sprintf("Error deleting value: %q", err))
			return
		}
	}
}
func main() {

	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatalf("Error opening database: %q", err)
	}

	Driver = db

	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "This is root directory",
		})
	})

	r.POST("/postitem", posts)
	r.GET("/getitem", retrievevalue(db))
	r.DELETE("/deleteitem", deletevalue(db))
	r.Run()
}
